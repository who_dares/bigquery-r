#Running queries on BQ Datasets

#Run through Start-up.R


#SQL Query

sql <- "####" #Insert query in SQL format.  Saves query to variable.

#Executing query and storing as data frame.

JOB <- bq_project_query(billing, sql) #Executes BQ job and stores as variable. Takes the previously created billing and sql variables as arguments

DATAFRAME <- bq_table_download(JOB, max_results = 10) #Downloads executed query as table to R Studio and stores as dataframe for analysis and visualisation. Takes JOB as argument. Downloaded rows can be limited using "max_results= " argument.